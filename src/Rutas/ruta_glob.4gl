GLOBALS 
DEFINE
    gdatabase STRING,
    gcompany STRING,
    reg_enca RECORD
        fecha_despacho DATE
    END RECORD

TYPE  type_datos_reporte RECORD
    reg_enca RECORD
        fecha DATE,
        ruta varchar(50,1),
        camion VARCHAR(50,1),
        proyecto VARCHAR(100,1),
        orden   VARCHAR(50,1),
        cliente VARCHAR(150,1)
    END RECORD,
    
    reg_det DYNAMIC ARRAY OF RECORD
        numero_linea SMALLINT,
        codigo_producto VARCHAR(100),
        nombre_producto VARCHAR(200),
        unidad_medida   VARCHAR(100),
        cantidad        DECIMAL(12,3)
    END RECORD

END RECORD

DEFINE reg_datos_reporte  type_datos_reporte;

    
END GLOBALS
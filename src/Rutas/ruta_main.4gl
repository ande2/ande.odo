GLOBALS "ruta_glob.4gl"


MAIN

INITIALIZE gdatabase, gcompany TO NULL

LET gdatabase = "pronisa"
LET gcompany  = 'PRONISA'

CONNECT TO gdatabase
SET CONNECTION gdatabase

OPTIONS INPUT WRAP
OPEN WINDOW forma1 WITH FORM ("futa_form")

CALL pide_parametros()

CLOSE WINDOW forma1
 
END MAIN

FUNCTION pide_parametros()
DEFINE cnt SMALLINT

    INITIALIZE cnt,reg_enca.* TO NULL
  
    LET reg_enca.fecha_despacho = TODAY
    
    CALL ui.Dialog.setDefaultUnbuffered(TRUE)
    CALL ui.Window.getCurrent().setText("Reporte de rutas")
    
    DIALOG
     
        INPUT BY NAME reg_enca.* ATTRIBUTES( WITHOUT DEFAULTS=TRUE)
            BEFORE INPUT
               

            AFTER INPUT
            
            
            ON ACTION Imprimir  
                CALL genera_reporte()
            
            ON ACTION Salir
                EXIT DIALOG

        END INPUT
               
    END DIALOG


END FUNCTION

FUNCTION genera_reporte()
DEFINE cnt SMALLINT
DEFINE handler om.SaxDocumentHandler

    INITIALIZE reg_datos_reporte.* TO NULL

    CALL obtiene_datos_reporte()

    IF fgl_report_loadCurrentSettings("ruta_report.4rp") THEN  
        CALL fgl_report_selectDevice("PDF")
        LET handler = fgl_report_commitCurrentSettings()     
    END IF 

    IF handler IS NOT NULL THEN   
        START REPORT imprime_reporte TO XML HANDLER HANDLER
        FOR cnt = 1 TO reg_datos_reporte.reg_det.getLength()
            IF reg_datos_reporte.reg_det[cnt].nombre_producto IS NOT NULL THEN
                OUTPUT TO REPORT imprime_reporte(reg_datos_reporte.reg_enca.*,reg_datos_reporte.reg_det[cnt].*)
            END IF
        END FOR
        FINISH REPORT imprime_reporte
    END IF


END FUNCTION

FUNCTION obtiene_datos_reporte()
DEFINE cnt SMALLINT

LET cnt = 1

DECLARE cur_datos CURSOR FOR
SELECT 
sale_order.commitment_date,
x_route.name,
x_truck.name,
x_project.name,
sale_order.client_order_ref,
res_partner.name,
0,
product_template.default_code,
sale_order_line.name, 
uom_uom.name,
sale_order_line.product_uom_qty

FROM sale_order, res_partner, x_route, x_truck, x_project, sale_order_line, Product_template, uom_uom
WHERE sale_order.commitment_date = gr_enca.fecha_despacho
AND sale_order.state IN ('sale','done')
AND res_partner.id = sale_order.partner_id
AND x_route.id = sale_order.x_route
AND x_truck.id = sale_order.x_truck
AND x_project.id = sale_order.x_project 
AND sale_order_line.order_id = sale_order.id
AND uom_uom.id = sale_order_line.product_uom
AND product_template.id = sale_order_line.product_id

FOREACH cur_datos INTO reg_datos_reporte.reg_enca.*, reg_datos_reporte.reg_det[cnt].*
    LET reg_datos_reporte.reg_det[cnt].numero_linea = cnt
    LET cnt = cnt + 1
END FOREACH  

END FUNCTION

REPORT imprime_reporte(rep_datos_reporte)
DEFINE rep_datos_reporte RECORD
    reg_enca RECORD
        fecha DATE,
        ruta varchar(50,1),
        camion VARCHAR(50,1),
        proyecto VARCHAR(100,1),
        orden   VARCHAR(50,1),
        cliente VARCHAR(150,1)
    END RECORD,
    
    reg_det RECORD
        numero_linea SMALLINT,
        codigo_producto VARCHAR(100),
        nombre_producto VARCHAR(200),
        unidad_medida   VARCHAR(100),
        cantidad        DECIMAL(12,3)
    END RECORD
END RECORD

    FORMAT 
    FIRST PAGE HEADER
       
        PRINTX rep_datos_reporte.reg_enca.*

    ON EVERY ROW
       PRINTX rep_datos_reporte.reg_det.* 


END REPORT

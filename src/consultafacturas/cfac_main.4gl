--IMPORT FGL greruntime
--IMPORT FGL gredesigntime

GLOBALS "cfac_glob.4gl"


MAIN

INITIALIZE gdatabase, gcompany, gjournal TO NULL

{LET gdatabase = arg_val(1)
LET gcompany = arg_val(2)
LET gjournal = arg_val(3)}

--LET gdatabase = 'pronisatest'
LET gdatabase = "pronisa"
LET gcompany  = 'PRONISA'
--LET gjournal  = 'INV'
LET gjournal  = 'FACC'

CONNECT TO gdatabase
CONNECT TO "ande"
SET CONNECTION gdatabase

OPTIONS INPUT WRAP
OPEN WINDOW forma1 WITH FORM ("cfac_form")

CALL pide_parametros()

CLOSE WINDOW forma1
 
END MAIN

FUNCTION pide_parametros()
DEFINE cnt SMALLINT
DEFINE cnt2 SMALLINT
DEFINE hay_seleccionados SMALLINT
DEFINE hay_errores SMALLINT


    INITIALIZE cnt,reg_enca.* TO NULL
    IF gcompany IS NOT NULL THEN
        SELECT id
        INTO reg_enca.company_id
        FROM res_company
        WHERE res_company.name = gcompany
    END IF

    IF gjournal IS NOT NULL THEN
        SELECT id
        INTO reg_enca.journal_id
        FROM account_journal
        WHERE account_journal.type = 'sale'
        AND account_journal.code = gjournal
    END IF
    
    LET gfecha_inicial = TODAY - 7 --MM,DD,YYYY
    LET reg_enca.fecha_ini = gfecha_inicial
    LET reg_enca.fecha_fin = TODAY
    CALL lib_combo("company_id","id","name","res_company","1=1")
    CALL lib_combo("journal_id","id","code","account_journal","type='sale'")
    CALL ui.Dialog.setDefaultUnbuffered(TRUE)
    CALL ui.Window.getCurrent().setText("Certificador de facturas")
    --CALL fgl_settitle("Certificador de facturas")
    DIALOG
        INPUT ARRAY det_facturas FROM scr_facturas.* ATTRIBUTES(WITHOUT DEFAULTS = TRUE,APPEND ROW=FALSE,DELETE ROW=FALSE,INSERT ROW=FALSE )
            BEFORE INPUT
                IF reg_enca.company_id IS NOT NULL AND
                   reg_enca.journal_id IS NOT NULL THEN
                    CALL carga_facturas()
                END IF
                NEXT FIELD sel
                
            BEFORE ROW
                LET cnt = arr_curr()
               -- CALL numeroALetras(det_facturas[cnt].montobase, det_facturas[cnt].moneda)

            ON ACTION Imprimir  
                CALL registra_factura(det_facturas[cnt].idfactura, cnt,2)  
                
            ON ACTION Certificar
                LET hay_seleccionados = 0
                LET hay_errores = 0
                FOR cnt2 = 1 TO det_facturas.getLength()
                    IF det_facturas[cnt2].sel = 1 THEN
                         IF det_facturas[cnt2].fechemision < gfecha_inicial AND 
                            det_facturas[cnt2].estado <> "ANULADO" THEN
                            LET hay_errores = 1
                        END IF
                        LET hay_seleccionados = hay_seleccionados + 1
                    END IF
                END FOR

                IF hay_seleccionados > 0 THEN
                    IF hay_errores = 1 THEN
                        ERROR "Existe una o varias facturas con fecha fuera de parametros para certificar"
                    ELSE
                        FOR cnt2 = 1 TO det_facturas.getLength()
                            IF det_facturas[cnt2].sel = 1 THEN
                                CALL registra_factura(det_facturas[cnt2].idfactura, cnt2,1)
                            END IF
                        END FOR
                    END IF 
                ELSE
                    IF det_facturas[cnt].fechemision < gfecha_inicial AND
                        det_facturas[cnt2].estado <> "ANULADO" THEN
                        ERROR "Fecha de la factura fuera de parametros para certificar"
                    ELSE
                        CALL registra_factura(det_facturas[cnt].idfactura, cnt,1)
                    END IF
                END IF
                
                CALL carga_facturas()

            ON ACTION Actualizar
                CALL carga_facturas()
               
            ON ACTION Salir
                EXIT DIALOG
        END INPUT 
        INPUT BY NAME reg_enca.* ATTRIBUTES( WITHOUT DEFAULTS=TRUE)
            BEFORE INPUT
                IF reg_enca.company_id IS NOT NULL AND
                   reg_enca.journal_id IS NOT NULL THEN
                    CALL carga_facturas()
                END IF
                
            ON CHANGE company_id
                IF reg_enca.company_id IS NOT NULL AND
                   reg_enca.journal_id IS NOT NULL THEN
                    CALL carga_facturas()
                END IF
                
            ON CHANGE journal_id
                IF reg_enca.company_id IS NOT NULL AND
                   reg_enca.journal_id IS NOT NULL THEN
                    CALL carga_facturas()
                END IF

            AFTER INPUT
                IF reg_enca.fecha_fin < reg_enca.fecha_ini THEN
                    ERROR "La fecha final no puede ser menor a la fecha inicial"
                    NEXT FIELD fecha_ini
                END IF 
            
            ON ACTION Actualizar
                IF reg_enca.company_id IS NOT NULL AND
                   reg_enca.journal_id IS NOT NULL THEN
                    CALL carga_facturas()
                ELSE
                    MESSAGE "Debe indcar la empresa y diário"
                END IF
            
            ON ACTION Salir
                EXIT DIALOG

        END INPUT
        
{
        DISPLAY ARRAY det_facturas TO scr_facturas.*
            BEFORE ROW
                LET cnt = arr_curr()
               -- CALL numeroALetras(det_facturas[cnt].montobase, det_facturas[cnt].moneda)

            ON ACTION Imprimir  
                CALL registra_factura(det_facturas[cnt].idfactura, cnt,2)  
                
            ON ACTION Certificar
                CALL registra_factura(det_facturas[cnt].idfactura, cnt,1)
                CALL carga_facturas()

            ON ACTION Actualizar
                CALL carga_facturas()
               
            ON ACTION Salir
                EXIT DIALOG
        END DISPLAY
 }       
    END DIALOG


END FUNCTION


FUNCTION lib_combo(vcampo_form,vcampo1,vcampo2,vtabla,vcondicion)
DEFINE vcampo_form VARCHAR(100)
DEFINE vcampo1 VARCHAR(100)
DEFINE vcampo2 VARCHAR(100)
DEFINE vtabla VARCHAR(100)
DEFINE vcondicion VARCHAR(100)

 DEFINE cb ui.ComboBox 

 LET cb = ui.ComboBox.forName(vcampo_form)

 IF cb IS NOT NULL THEN
    CALL loadcb(cb,vcampo1,vcampo2,vtabla,vcondicion)
 END IF

END FUNCTION

FUNCTION loadcb(cb,vcampo1,vcampo2,vtabla,vcondicion)
  DEFINE cb ui.ComboBox,
         l_code INTEGER,
         l_name VARCHAR(100)
    DEFINE vcampo1 VARCHAR(100)
    DEFINE vcampo2 VARCHAR(100)
    DEFINE vtabla VARCHAR(100)
    DEFINE vcondicion VARCHAR(100)
    DEFINE vsql STRING

  LET vsql = "SELECT "||vcampo1||","||vcampo2||" FROM "||vtabla||" WHERE "||vcondicion
  PREPARE rep_qry1 FROM vsql

  DECLARE mycurs CURSOR FOR rep_qry1
   
  CALL cb.clear()
  FOREACH mycurs INTO l_code, l_name 
  -- provide name and text for the ComboBox item 
   CALL cb.addItem(l_code,l_name)
  END FOREACH
 END FUNCTION

FUNCTION carga_facturas()

DEFINE cnt SMALLINT
DEFINE vfecha_tipo_cambio DATE
--DEFINE vfecha_inicial DATE

LET cnt = 1
CALL det_facturas.clear()

LET vfecha_inicial = MDY(MONTH(gfecha_inicial),1,YEAR(gfecha_inicial))

DECLARE cur_facturas CURSOR FOR
SELECT 
0,
account_move.sequence_prefix, 
account_move.sequence_number, 
0,
0,
account_journal.code, --descripción del diario
account_move.invoice_date,
account_move.invoice_date,    --pendiente fecha anulacion
account_move.state,  --descrición de estado
res_currency.name, -- nombre moneda
1, --pendiente traer tipo de cambio
res_partner.name, --nombre cliente
account_move.amount_untaxed, --monto base
account_move.amount_tax, --monto iva
0, --monto descuento
account_move.amount_total, --monto total
0, --account_move.name,
'FC', --tipo docto
account_move.x_estatus_fel, --'P', --estado_fel
account_move.x_serie_fel, --'', --serie_fel
account_move.x_numero_fel, --'', --numero_fel
account_move.x_autorizacion_fel, --'', --autorizacion fel
account_move.id,
account_move.journal_id,
account_move.currency_id,
account_move.partner_id,
account_move.state,
account_move.x_total_letras,
account_move.invoice_origin,
account_move.x_fecha_fel,
account_move.invoice_incoterm_id,
account_move.fiscal_position_id,
account_move.x_certificador_nit,
account_move.x_certificador_nombre,
account_move.x_estado_certanl
FROM account_move, account_journal, res_currency, res_partner
WHERE account_move.invoice_date BETWEEN reg_enca.fecha_ini AND reg_enca.fecha_fin
--WHERE account_move.invoice_date >= vfecha_inicial
AND account_move.state = 'posted'
AND account_move.move_type IN ('out_invoice')
AND account_move.amount_total_signed > 0
AND account_move.journal_id = reg_enca.journal_id
AND account_move.company_id = reg_enca.company_id
AND account_journal.id = account_move.journal_id
AND res_currency.id = account_move.currency_id
AND res_partner.id = account_move.partner_id

UNION ALL 
SELECT 
0,
account_move.sequence_prefix, 
account_move.sequence_number,
0,
0, 
account_journal.code, --descripción del diario
account_move.invoice_date,
DATE(account_move.write_date),    --pendiente fecha anulacion
account_move.state,  --descrición de estado
res_currency.name, -- nombre moneda
1, --pendiente traer tipo de cambio
res_partner.name, --nombre cliente
account_move.amount_untaxed, --monto base
account_move.amount_tax, --monto iva
0, --monto descuento
account_move.amount_total, --monto total
account_move.x_fac_idfel, --idfactura
'FC', --tipo docto
account_move.x_estatus_fel, --'P', --estado_fel
account_move.x_serie_fel, --'', --serie_fel
account_move.x_numero_fel, --'', --numero_fel
account_move.x_autorizacion_fel, --'', --autorizacion fel
account_move.id,
account_move.journal_id,
account_move.currency_id,
account_move.partner_id,
account_move.state,
account_move.x_total_letras,
account_move.invoice_origin,
account_move.x_fecha_fel,
account_move.invoice_incoterm_id,
account_move.fiscal_position_id,
account_move.x_certificador_nit,
account_move.x_certificador_nombre,
account_move.x_estado_certanl
FROM account_move, account_journal, res_currency, res_partner
--WHERE DATE(account_move.write_date) >= vfecha_inicial 
WHERE account_move.invoice_date BETWEEN reg_enca.fecha_ini AND reg_enca.fecha_fin
AND account_move.state = 'cancel'
AND account_move.x_autorizacion_fel IS NOT NULL
AND account_move.move_type IN ('out_invoice')
AND account_move.amount_total_signed > 0
AND account_move.journal_id = reg_enca.journal_id
AND account_move.company_id = reg_enca.company_id
AND account_journal.id = account_move.journal_id
AND res_currency.id = account_move.currency_id
AND res_partner.id = account_move.partner_id

ORDER BY 2, 3 desc


FOREACH cur_facturas INTO det_facturas[cnt].*

    IF det_facturas[cnt].state = "posted" THEN
        LET det_facturas[cnt].estado = "ORIGINAL"
        ELSE
            IF det_facturas[cnt].state = "cancel" THEN
                LET det_facturas[cnt].estado = "ANULADO"
            ELSE
                LET det_facturas[cnt].estado = "NO VALIDO"
            END IF
    END IF
    IF det_facturas[cnt].totalenletras IS NULL THEN
        CALL numeroALetras(det_facturas[cnt].montototal, det_facturas[cnt].moneda) RETURNING det_facturas[cnt].totalenletras
    END IF

      IF det_facturas[cnt].moneda = "USD" THEN --si es moneda dolares obtiene tipo de cambio
        SELECT MAX(res_currency_rate.name)
        INTO vfecha_tipo_cambio
        FROM res_currency_rate
        WHERE res_currency_rate.currency_id = 2
        AND res_currency_rate.name <= det_facturas[cnt].fechemision

        SELECT res_currency_rate.rate
        INTO det_facturas[cnt].tipocambio
        FROM res_currency_rate
        WHERE res_currency_rate.currency_id = 2 --dolares
        AND res_currency_rate.name = vfecha_tipo_cambio

        IF SQLCA.SQLCODE = NOTFOUND THEN
            LET det_facturas[cnt].tipocambio = 1
        ELSE
            IF det_facturas[cnt].tipocambio > 0 THEN
                LET det_facturas[cnt].tipocambio = 1 / det_facturas[cnt].tipocambio
            END IF
        END IF

    END IF
    IF det_facturas[cnt].estado_fel = "C" THEN
        LET det_facturas[cnt].certificaciondte = 1
    END IF

    IF det_facturas[cnt].anulcert = "C" THEN
        LET det_facturas[cnt].anulaciondte = 1
    END IF
    
    DISPLAY "CNT: ",cnt, " certificador_nit: ",det_facturas[cnt].certificador_nit," nombre: ",det_facturas[cnt].certificador_nombre
    LET cnt = cnt + 1
END FOREACH

DISCONNECT gdatabase

WHENEVER ERROR CONTINUE
CONNECT TO "ande"
WHENEVER ERROR STOP
IF sqlca.sqlcode = -1802 THEN
    SET CONNECTION "ande"
END IF

{
FOR cnt = 1 TO det_facturas.getLength()
    IF det_facturas[cnt].serie IS NOT NULL THEN
        SELECT facturafel_e.estatus, facturafel_e.serie_e, facturafel_e.numdoc_e, facturafel_e.autorizacion
        INTO det_facturas[cnt].estado_fel, det_facturas[cnt].serie_fel, det_facturas[cnt].numero_fel, det_facturas[cnt].autorizacionfel
        FROM facturafel_e
        WHERE facturafel_e.serie = det_facturas[cnt].serie
        AND facturafel_e.num_doc = det_facturas[cnt].numero
        AND facturafel_e.tipod = "FC"
    END IF
    
END FOR
}
DISCONNECT "ande"
WHENEVER ERROR CONTINUE
CONNECT TO gdatabase
WHENEVER ERROR STOP
IF sqlca.sqlcode = -1802 THEN
        SET CONNECTION gdatabase
END IF


END FUNCTION

FUNCTION registra_factura(vid, cnt, certificaimprime)
DEFINE vid INTEGER
DEFINE cnt SMALLINT 
DEFINE certificaimprime SMALLINT --1=Certifica factura 2=Imprime factura
DEFINE vrespuesta SMALLINT
DEFINE vfac_id INTEGER
DEFINE vcomando STRING
DEFINE vcorrelativolog INTEGER
DEFINE reg_certificacion RECORD
    serie_e VARCHAR(20),
    numdoc_e VARCHAR(40),
    autorizacion VARCHAR(200),
    estatus CHAR(2),
    fecha_certificacion DATETIME YEAR TO FRACTION,
    certificador_nit VARCHAR(50),
    certificador_nombre VARCHAR(100),
    mensaje VARCHAR(200),
    fac_idfel INTEGER,
    certificador_fecha VARCHAR(35)
END RECORD
DEFINE handler om.SaxDocumentHandler

    INITIALIZE reg_datos_factura.* TO NULL
    CALL obtiene_datos_compania()
    CALL obtiene_datos_cliente(det_facturas[cnt].partner_id)
    CALL obtiene_encabezado_factura(cnt)
    CALL obtiene_detalle_factura(det_facturas[cnt].move_id)

    IF certificaimprime = 1 THEN --certificar factura
        CONNECT TO "ande"

        BEGIN WORK
        CALL registra_tablas_fel() RETURNING vrespuesta, vfac_id
DISPLAY "REPUESTA: ",vrespuesta," VFAC_ID: ",vfac_id
        IF vrespuesta = 0 THEN
            COMMIT WORK
            LET vcomando = "fglrun ande_fel ",vfac_id using "<<<<<<", " ande 0 0"
            DISPLAY "vcomando: ",vcomando
            RUN vcomando
            
            --Obtiene datos de firma electronica de factura y lo registra en tablas de odoo
            INITIALIZE reg_certificacion.* TO NULL
            SELECT facturafel_e.serie_e, facturafel_e.numdoc_e, facturafel_e.autorizacion, facturafel_e.estatus,
                   facturafel_e.fac_id
            INTO reg_certificacion.serie_e, reg_certificacion.numdoc_e, reg_certificacion.autorizacion, reg_certificacion.estatus,
                 reg_certificacion.fac_idfel
            FROM facturafel_e 
            WHERE facturafel_e.fac_id = vfac_id

            LET vcorrelativolog = NULL
            SELECT MAX(correlativo)
            INTO vcorrelativolog
            FROM factura_log
            WHERE factura_log.fac_id = vfac_id

            SELECT factura_log.certificador_fecha, factura_log.certificador_nit, factura_log.certificador_nombre, factura_log.msg_error
            INTO reg_certificacion.certificador_fecha, reg_certificacion.certificador_nit, reg_certificacion.certificador_nombre, reg_certificacion.mensaje
            FROM factura_log
            WHERE factura_log.fac_id = vfac_id
            AND factura_log.correlativo = vcorrelativolog

            LET reg_certificacion.fecha_certificacion =reg_certificacion.certificador_fecha[1,10]||" "||reg_certificacion.certificador_fecha[12,19]
        ELSE
            ROLLBACK WORK
            INITIALIZE reg_certificacion.* TO NULL
        END IF
    
        DISCONNECT "ande"
        SET CONNECTION gdatabase

        IF vrespuesta = 0 THEN
            BEGIN WORK
            IF reg_certificacion.estatus = "C" THEN
                IF det_facturas[cnt].estado = "ANULADO" THEN
                    UPDATE account_move
                    SET x_estado_certanl = reg_certificacion.estatus,
                        x_fecha_certanl = reg_certificacion.fecha_certificacion
                    WHERE id = det_facturas[cnt].move_id
                    MESSAGE "La anulación de la factura ",det_facturas[cnt].serie CLIPPED,"-",det_facturas[cnt].numero USING "<<<<<<<<<"," ha sido certificada"
                    SLEEP 1
                ELSE
                    UPDATE account_move
                    SET x_serie_fel = reg_certificacion.serie_e,
                    x_numero_fel = reg_certificacion.numdoc_e,
                    x_autorizacion_fel = reg_certificacion.autorizacion,
                    x_estatus_fel = reg_certificacion.estatus,
                    x_total_letras = det_facturas[cnt].totalenletras,
                    x_fecha_fel = reg_certificacion.fecha_certificacion,
                    x_tipo_cambio = det_facturas[cnt].tipocambio,
                    x_certificador_nit = reg_certificacion.certificador_nit,
                    x_certificador_nombre = reg_certificacion.certificador_nombre,
                    x_fac_idfel = reg_certificacion.fac_idfel,
                    x_project = reg_datos_factura.reg_factura.proyecto
                    WHERE id = det_facturas[cnt].move_id
                    MESSAGE "La factura ",det_facturas[cnt].serie CLIPPED,"-",det_facturas[cnt].numero USING "<<<<<<<<<"," ha sido certificada"
                    SLEEP 1
                END IF
            ELSE 
            ERROR  "No se pudo certificar la factura ",det_facturas[cnt].serie CLIPPED,"-",det_facturas[cnt].numero USING "<<<<<<<<<"
            SLEEP 1
            ERROR "Error: ",reg_certificacion.mensaje
            SLEEP 1
            END IF
            COMMIT WORK
        END IF
    END IF
    
    IF certificaimprime = 2 THEN --IMPRIME

        IF fgl_report_loadCurrentSettings("cfac_report.4rp") THEN  
            CALL fgl_report_selectDevice("PDF")
            LET handler = fgl_report_commitCurrentSettings()     
        END IF 
 
 
         IF handler IS NOT NULL THEN   
            START REPORT imprime_factura TO XML HANDLER HANDLER
            FOR cnt = 1 TO reg_datos_factura.reg_det_factura.getLength()
                IF reg_datos_factura.reg_det_factura[cnt].nombre_producto IS NOT NULL THEN
                    OUTPUT TO REPORT imprime_factura(reg_datos_factura.reg_company.*,reg_datos_factura.reg_factura.*,reg_datos_factura.reg_cliente.*,reg_datos_factura.reg_det_factura[cnt].*)
                END IF
            END FOR
            FINISH REPORT imprime_factura
        END IF

    END IF
END FUNCTION

FUNCTION obtiene_datos_compania()
DISCONNECT gdatabase

WHENEVER ERROR CONTINUE
CONNECT TO "ande" 
IF sqlca.sqlcode = -1802 THEN
        SET CONNECTION "ande"
END IF
WHENEVER ERROR STOP

    SELECT empresas1.nit,
   -- res_company.x_studio_afiliacion_iva,
    empresas1.razon_social,
    empresas1.cod_sucursal,
    empresas1.nom_sucursal,
    empresas1.e_mail,
    empresas1.direccion1,
    empresas1.direccion2,
    empresas1.codigo_postal,
    empresas1.municipio,
    empresas1.depto,
    "GT",
    empresas1.exportador_nombre,
    empresas1.exportador_codigo
    INTO reg_datos_factura.reg_company.x_studio_nit,
    --reg_datos_factura.reg_company.x_studio_afiliacion_iva,
    reg_datos_factura.reg_company.x_studio_nombre_comercial,
    reg_datos_factura.reg_company.x_studio_codigo_establecimiento,
    reg_datos_factura.reg_company.x_studio_nombre_del_establecimiento,
    reg_datos_factura.reg_company.x_studio_email_fel,
    reg_datos_factura.reg_company.street,
    reg_datos_factura.reg_company.street2,
    reg_datos_factura.reg_company.zip,
    reg_datos_factura.reg_company.city,
    reg_datos_factura.reg_company.departamento_establecimiento,
    reg_datos_factura.reg_company.pais_establecimiento,
    reg_datos_factura.reg_company.exportador_nombre,
    reg_datos_factura.reg_company.exportador_codigo
    FROM empresas1
    WHERE empresas1.nom_sucursal = gcompany
    
    
    LET reg_datos_factura.reg_company.direccion_establecimiento = NVL(reg_datos_factura.reg_company.street," ") || " " || NVL(reg_datos_factura.reg_company.street2," ")
    LET reg_datos_factura.reg_company.municipio_establecimiento = reg_datos_factura.reg_company.city
    LET reg_datos_factura.reg_company.codigo_postal_establecimiento = reg_datos_factura.reg_company.zip
    
    DISCONNECT "ande"
    CONNECT TO gdatabase
   -- SET CONNECTION gdatabase
END FUNCTION

FUNCTION obtiene_datos_cliente(vid)
DEFINE vid  INTEGER

    SELECT res_partner.email,
    res_partner.vat,
    res_partner.name,
    res_partner.street,res_partner.street2,
    res_country.code,
    res_country_state.name,
    res_partner.city,
    res_partner.zip,
    res_partner.commercial_partner_id
    INTO reg_datos_factura.reg_cliente.email_cliente,
    reg_datos_factura.reg_cliente.nit_cliente,
    reg_datos_factura.reg_cliente.nombre_cliente,
    reg_datos_factura.reg_cliente.street,
    reg_datos_factura.reg_cliente.street2,
    reg_datos_factura.reg_cliente.pais_cliente,
    reg_datos_factura.reg_cliente.nombre_departamento_cliente,
    reg_datos_factura.reg_cliente.municipio_cliente,
    reg_datos_factura.reg_cliente.codigo_postal_cliente,
    reg_datos_factura.reg_cliente.vendedor_id
    FROM res_partner , res_country, OUTER(res_country_state)
    WHERE res_partner.id = vid
    AND res_country.id = res_partner.country_id
    AND res_country_state.id = res_partner.state_id
   
    LET reg_datos_factura.reg_cliente.tipo_documento_identificacion = 0 --0=nit 1=CUI
    LET reg_datos_factura.reg_cliente.direccion_cliente = NVL(reg_datos_factura.reg_cliente.street," ") || " " || NVL(reg_datos_factura.reg_cliente.street2," ")

END FUNCTION

FUNCTION obtiene_encabezado_factura(cnt)
DEFINE cnt SMALLINT

    LET reg_datos_factura.reg_factura.serie = det_facturas[cnt].serie
    LET reg_datos_factura.reg_factura.numero = det_facturas[cnt].numero
    LET reg_datos_factura.reg_factura.diario = det_facturas[cnt].diario
    LET reg_datos_factura.reg_factura.tipo_factura = "FCAM"
    IF reg_datos_factura.reg_factura.estado_factura = "ANULADO" THEN
        LET reg_datos_factura.reg_factura.fecha_factura = TODAY
        LET reg_datos_factura.reg_factura.fecha_anulacion = TODAY USING "yyyy-mm-dd"||"T"||CURRENT HOUR TO SECOND
    ELSE
        LET reg_datos_factura.reg_factura.fecha_factura = det_facturas[cnt].fechemision
        LET reg_datos_factura.reg_factura.fecha_anulacion = det_facturas[cnt].fechaanulacion USING "yyyy-mm-dd"||"T"||CURRENT HOUR TO SECOND
    END IF
    LET reg_datos_factura.reg_factura.fecha_emision = TODAY USING "yyyy-mm-dd"||"T"||CURRENT HOUR TO SECOND
    
    LET reg_datos_factura.reg_factura.estado_factura = det_facturas[cnt].estado
    LET reg_datos_factura.reg_factura.moneda = det_facturas[cnt].moneda
    LET reg_datos_factura.reg_factura.tipo_cambio = det_facturas[cnt].tipocambio
    
    IF reg_datos_factura.reg_factura.estado_factura = "ANULADO" THEN
        LET reg_datos_factura.reg_factura.monto_base =  NULL
        LET reg_datos_factura.reg_factura.monto_iva = NULL
        LET reg_datos_factura.reg_factura.monto_descuento = NULL 
        LET reg_datos_factura.reg_factura.monto_total = NULL
        LET reg_datos_factura.reg_factura.total_en_letras = NULL
    ELSE
        LET reg_datos_factura.reg_factura.monto_base =  det_facturas[cnt].montobase
        --LET reg_datos_factura.reg_factura.monto_iva = det_facturas[cnt].montoiva
        IF det_facturas[cnt].montototal = det_facturas[cnt].montobase THEN
            LET reg_datos_factura.reg_factura.monto_iva = 0
        ELSE
            LET reg_datos_factura.reg_factura.monto_iva = (det_facturas[cnt].montototal/1.12)*0.12
        END IF
        LET reg_datos_factura.reg_factura.monto_descuento = det_facturas[cnt].montodescuento
        LET reg_datos_factura.reg_factura.monto_total = det_facturas[cnt].montototal
        LET reg_datos_factura.reg_factura.monto_base =  reg_datos_factura.reg_factura.monto_total - reg_datos_factura.reg_factura.monto_iva 
        LET reg_datos_factura.reg_factura.total_en_letras = det_facturas[cnt].totalenletras
    END IF
    LET reg_datos_factura.reg_factura.estado_certificado = "P"

    IF det_facturas[cnt].idfactura = 0 THEN
        LET reg_datos_factura.reg_factura.id_factura = NULL
    ELSE
        LET reg_datos_factura.reg_factura.id_factura = det_facturas[cnt].idfactura
    END IF
    
    LET reg_datos_factura.reg_factura.nombre_consignatario_destinatario = reg_datos_factura.reg_cliente.nombre_cliente
    LET reg_datos_factura.reg_factura.direccion_consignatario_destinatario = reg_datos_factura.reg_cliente.direccion_cliente
    LET reg_datos_factura.reg_factura.incoterm = ""
    LET reg_datos_factura.reg_factura.serial = 0
    LET reg_datos_factura.reg_factura.es_exenta = 0
    LET reg_datos_factura.reg_factura.posicion_fiscal = det_facturas[cnt].posicion_fiscal
    
    IF reg_datos_factura.reg_company.pais_establecimiento = reg_datos_factura.reg_cliente.pais_cliente THEN
        LET reg_datos_factura.reg_factura.es_exportacion = 0
        IF reg_datos_factura.reg_factura.posicion_fiscal = 1 THEN
            SELECT account_fiscal_position.x_frace, account_fiscal_position.x_escenario
            INTO reg_datos_factura.reg_factura.cod_frase, reg_datos_factura.reg_factura.cod_escenario
            FROM account_fiscal_position
            WHERE account_fiscal_position.id = reg_datos_factura.reg_factura.posicion_fiscal

            IF reg_datos_factura.reg_factura.cod_frase = 4 AND reg_datos_factura.reg_factura.cod_escenario = 11 THEN --es 29-89
                LET reg_datos_factura.reg_factura.es_exenta = 1
            END IF
        END IF
       
    ELSE
        LET reg_datos_factura.reg_factura.es_exportacion = 1
        LET reg_datos_factura.reg_cliente.tipo_documento_identificacion = 2
        LET reg_datos_factura.reg_factura.es_exenta = 0
        
        SELECT account_incoterms.code
        INTO reg_datos_factura.reg_factura.incoterm
        FROM account_incoterms
        WHERE account_incoterms.id = det_facturas[cnt].invoice_incoterm_id
    END IF


    SELECT account_payment_term.name
    INTO reg_datos_factura.reg_factura.forma_de_pago
    FROM sale_order, account_payment_term
    WHERE sale_order.name = det_facturas[cnt].invoice_origin
    AND account_payment_term.id = sale_order.payment_term_id

    SELECT sale_order.client_order_ref, sale_order.x_project
    INTO reg_datos_factura.reg_factura.numero_pedido, reg_datos_factura.reg_factura.proyecto
    FROM sale_order
    WHERE sale_order.name = det_facturas[cnt].invoice_origin

    SELECT res_partner.name
    INTO reg_datos_factura.reg_factura.vendedor
    FROM res_partner
    WHERE res_partner.id = reg_datos_factura.reg_cliente.vendedor_id
    
    LET reg_datos_factura.reg_factura.tipo_documento = "FC"
    IF reg_datos_factura.reg_factura.estado_factura = "ANULADO" THEN
        LET reg_datos_factura.reg_factura.estado_del_documento = "ANULADO"
    ELSE
        LET reg_datos_factura.reg_factura.estado_del_documento = "ORIGINAL"
    END IF
    
    IF reg_datos_factura.reg_factura.numero_pedido IS NULL THEN
        LET reg_datos_factura.reg_factura.numero_pedido = det_facturas[cnt].invoice_origin
    END IF

    SELECT account_move.name
    INTO reg_datos_factura.reg_factura.numero_interno
    FROM account_move
    WHERE account_move.id = det_facturas[cnt].move_id
    
    LET reg_datos_factura.reg_factura.ruta = "" --no se maneja
    
    SELECT COUNT(*)
    INTO reg_datos_factura.reg_factura.total_de_productos
    FROM account_move_line
    WHERE account_move_line.move_id = det_facturas[cnt].move_id
    AND account_move_line.tax_line_id IS NULL
    AND account_move_line.debit = 0
    
    LET reg_datos_factura.reg_factura.observaciones = "" --pendiente obtener datos de la orden de compra
    LET reg_datos_factura.reg_factura.factura_envio = "F"
    LET reg_datos_factura.reg_factura.autorizacion_fel = det_facturas[cnt].autorizacionfel
    LET reg_datos_factura.reg_factura.serie_fel = det_facturas[cnt].serie_fel
    LET reg_datos_factura.reg_factura.numero_fel = det_facturas[cnt].numero_fel
    LET reg_datos_factura.reg_factura.estado_fel = det_facturas[cnt].estado_fel
    LET reg_datos_factura.reg_factura.fecha_fel = det_facturas[cnt].fecha_fel
    LET reg_datos_factura.reg_factura.fecha_feltxt = DATE(reg_datos_factura.reg_factura.fecha_fel) USING "yyyy-mm-dd"||"T"||TIME(reg_datos_factura.reg_factura.fecha_fel) 
  --  LET reg_datos_factura.reg_factura.fecha_emision = reg_datos_factura.reg_factura.fecha_feltxt 
    LET reg_datos_factura.reg_factura.ruta = reg_datos_factura.reg_factura.serie_fel CLIPPED||"-"||reg_datos_factura.reg_factura.numero_fel CLIPPED
    LET reg_datos_factura.reg_factura.certificador_nit = det_facturas[cnt].certificador_nit
    LET reg_datos_factura.reg_factura.certificador_nombre = det_facturas[cnt].certificador_nombre
END FUNCTION

FUNCTION obtiene_detalle_factura(vmove_id)
DEFINE vmove_id INTEGER
DEFINE numero_linea SMALLINT

    LET numero_linea = 1

    DECLARE cur_det_facturas CURSOR FOR
    SELECT 0, account_move_line.product_id, 
    product_template.default_code,
    account_move_line.name,
    uom_uom.name,
    account_move_line.quantity,
    account_move_line.price_unit,
    (account_move_line.price_total+account_move_line.discount),
    account_move_line.discount,
    account_move_line.price_total,
    account_move_line.price_subtotal,
    (account_move_line.price_total-account_move_line.price_subtotal),
    product_template.detailed_type,
    "IVA"
    FROM account_move_line, OUTER(product_template), OUTER(uom_uom)
    WHERE account_move_line.move_id = vmove_id
    AND account_move_line.tax_line_id IS NULL
    AND account_move_line.debit = 0
    AND product_template.id = account_move_line.product_id
    AND uom_uom.id = account_move_line.product_uom_id

    FOREACH cur_det_facturas INTO reg_datos_factura.reg_det_factura[numero_linea].*
        LET reg_datos_factura.reg_det_factura[numero_linea].numero_linea = numero_linea
        IF reg_datos_factura.reg_det_factura[numero_linea].tipo_producto IS NULL THEN
            LET reg_datos_factura.reg_det_factura[numero_linea].tipo_producto = "service"
        END IF
       
        IF reg_datos_factura.reg_det_factura[numero_linea].tipo_producto = "service" OR
        reg_datos_factura.reg_det_factura[numero_linea].tipo_producto = "Service" OR
        reg_datos_factura.reg_det_factura[numero_linea].tipo_producto = "Servicio" OR
        reg_datos_factura.reg_det_factura[numero_linea].tipo_producto = "servicio" THEN
            LET reg_datos_factura.reg_det_factura[numero_linea].tipo_producto = "S"
        ELSE
            LET reg_datos_factura.reg_det_factura[numero_linea].tipo_producto = "B"
        END IF
        
        IF reg_datos_factura.reg_det_factura[numero_linea].unidad_medida = "Units" THEN
            LET reg_datos_factura.reg_det_factura[numero_linea].unidad_medida = "Uni"
        END IF 
        LET numero_linea = numero_linea + 1
    END FOREACH

    FREE cur_det_facturas
END FUNCTION

FUNCTION registra_tablas_fel()
DEFINE cnt SMALLINT
DEFINE vfac_id INTEGER
DEFINE vestatus CHAR(2)

INITIALIZE vfac_id TO NULL 

    LET vestatus = NULL
    IF reg_datos_factura.reg_factura.estado_factura = "ANULADO" THEN
        SELECT facturafel_e.estatus, facturafel_e.fac_id
        INTO vestatus, vfac_id
        FROM facturafel_e
        WHERE facturafel_e.serie = reg_datos_factura.reg_factura.serie
        AND facturafel_e.num_doc = reg_datos_factura.reg_factura.numero
        AND facturafel_e.tipod = reg_datos_factura.reg_factura.tipo_documento
        AND facturafel_e.estado_doc = "ANULADO"
    ELSE
        SELECT facturafel_e.estatus, facturafel_e.fac_id
        INTO vestatus, vfac_id
        FROM facturafel_e
        WHERE facturafel_e.serie = reg_datos_factura.reg_factura.serie
        AND facturafel_e.num_doc = reg_datos_factura.reg_factura.numero
        AND facturafel_e.tipod = reg_datos_factura.reg_factura.tipo_documento
        AND facturafel_e.estado_doc = "ORIGINAL"
    END IF

    IF vestatus = "C" THEN
        ERROR "Factura ya certificada"
        RETURN -1, 0
    ELSE 
        IF vestatus = "P" THEN
            DELETE
            FROM facturafel_ed
            WHERE facturafel_ed.fac_id = vfac_id
        
            DELETE
            FROM facturafel_e
            WHERE facturafel_e.fac_id = vfac_id
        ELSE
            LET vfac_id = NULL
            SELECT MAX(fac_id)
            INTO vfac_id
            FROM facturafel_e

            IF vfac_id IS NULL THEN
                LET vfac_id = 1
            ELSE
                LET vfac_id = vfac_id + 1
            END IF
        
        END IF
    END IF

    INSERT INTO facturafel_e (
        fac_id, serie, num_doc, tipod, fecha, fecha_em, fecha_anul, mail_from,
        mail_to, cc , tipo_doc, estado_doc, c_moneda, tipo_cambio, nit_e ,
        nombre_c, nombre_e, codigo_e , direccion_e ,
        departamento_e, municipio_e, pais_e , codpos_e , nit_r , es_cui ,
        nombre_r, direccion_r, departamento_r, municipio_r, pais_r ,
        codpos_r, base1, monto1, total_neto, 
        estatus,  total_en_letras, 
        es_exportacion, incoterm,  o_vendedor, o_pedido, es_exenta,
        cod_frase, cod_escenario, id_factura
    ) 
    VALUES(vfac_id, reg_datos_factura.reg_factura.serie, reg_datos_factura.reg_factura.numero,
    reg_datos_factura.reg_factura.tipo_documento, reg_datos_factura.reg_factura.fecha_factura,
    reg_datos_factura.reg_factura.fecha_emision, reg_datos_factura.reg_factura.fecha_anulacion,
    reg_datos_factura.reg_company.x_studio_email_fel, reg_datos_factura.reg_cliente.email_cliente,
    reg_datos_factura.reg_cliente.email_adicional , 
    reg_datos_factura.reg_factura.tipo_factura, reg_datos_factura.reg_factura.estado_del_documento,
    reg_datos_factura.reg_factura.moneda, reg_datos_factura.reg_factura.tipo_cambio,
    reg_datos_factura.reg_company.x_studio_nit , reg_datos_factura.reg_company.x_studio_nombre_comercial,
    reg_datos_factura.reg_company.x_studio_nombre_del_establecimiento, reg_datos_factura.reg_company.x_studio_codigo_establecimiento ,
    reg_datos_factura.reg_company.direccion_establecimiento, reg_datos_factura.reg_company.departamento_establecimiento,
    reg_datos_factura.reg_company.municipio_establecimiento, reg_datos_factura.reg_company.pais_establecimiento,
    reg_datos_factura.reg_company.codigo_postal_establecimiento , reg_datos_factura.reg_cliente.nit_cliente ,
    reg_datos_factura.reg_cliente.tipo_documento_identificacion , reg_datos_factura.reg_cliente.nombre_cliente,
    reg_datos_factura.reg_cliente.direccion_cliente, reg_datos_factura.reg_cliente.nombre_departamento_cliente,
    reg_datos_factura.reg_cliente.municipio_cliente, reg_datos_factura.reg_cliente.pais_cliente ,
    reg_datos_factura.reg_cliente.codigo_postal_cliente,
    reg_datos_factura.reg_factura.monto_base, reg_datos_factura.reg_factura.monto_iva,
    reg_datos_factura.reg_factura.monto_total,
    reg_datos_factura.reg_factura.estado_certificado,
    reg_datos_factura.reg_factura.total_en_letras, reg_datos_factura.reg_factura.es_exportacion,
    reg_datos_factura.reg_factura.incoterm, reg_datos_factura.reg_factura.vendedor,
    reg_datos_factura.reg_factura.numero_pedido, reg_datos_factura.reg_factura.es_exenta,
    reg_datos_factura.reg_factura.cod_frase, reg_datos_factura.reg_factura.cod_escenario,
    reg_datos_factura.reg_factura.id_factura
    )

    IF SQLCA.SQLCODE < 0 THEN
        DISPLAY "Error al insertar el encabezado de facgtura FEL"
    ELSE
        SELECT MAX(facturafel_e.fac_id)
        INTO vfac_id
        FROM facturafel_e
        WHERE facturafel_e.serie = reg_datos_factura.reg_factura.serie
        AND facturafel_e.num_doc = reg_datos_factura.reg_factura.numero
        AND facturafel_e.tipod = reg_datos_factura.reg_factura.tipo_documento
    
        FOR cnt = 1 TO reg_datos_factura.reg_det_factura.getLength()
            IF reg_datos_factura.reg_det_factura[cnt].tipo_producto IS NOT NULL THEN
            INSERT INTO facturafel_ed (
                fac_id, serie, num_doc, descrip_p, codigo_p,
                unidad_m, cantidad, precio_u, precio_t, descto_t, tipo,
                base, monto, categoria
            )
            VALUES(
            vfac_id,reg_datos_factura.reg_factura.serie, reg_datos_factura.reg_factura.numero, 
            reg_datos_factura.reg_det_factura[cnt].nombre_producto, reg_datos_factura.reg_det_factura[cnt].codigo_producto,
            reg_datos_factura.reg_det_factura[cnt].unidad_medida, reg_datos_factura.reg_det_factura[cnt].cantidad,
            reg_datos_factura.reg_det_factura[cnt].precio_unitario, reg_datos_factura.reg_det_factura[cnt].valor_total_despues_descuento,
            reg_datos_factura.reg_det_factura[cnt].descuento,reg_datos_factura.reg_det_factura[cnt].nombre_impuesto, reg_datos_factura.reg_det_factura[cnt].valor_total_despues_descuento_sin_iva,
            reg_datos_factura.reg_det_factura[cnt].valor_del_iva, reg_datos_factura.reg_det_factura[cnt].tipo_producto
            )
            END IF
        END FOR
   END IF 

   RETURN 0, vfac_id
END FUNCTION

FUNCTION numeroALetras(vmonto, vmoneda)
DEFINE vmonto DECIMAL(18,2)
DEFINE vmoneda CHAR(3)
DEFINE vmonto_entero DECIMAL(18,0)
DEFINE vcentavos SMALLINT
DEFINE vmonto_string VARCHAR(12)
DEFINE vnuevo_monto_string VARCHAR(12)

DEFINE vnumero SMALLINT;
DEFINE vtexto VARCHAR(200);
DEFINE vbandera SMALLINT
DEFINE valor_letras VARCHAR(200)
DEFINE cnt,cnt_hasta SMALLINT

INITIALIZE valor_letras TO NULL

LET vmonto_entero = fgl_decimal_truncate(vmonto,0)

LET vcentavos = (vmonto - vmonto_entero) *100 ;

LET vmonto_string = vmonto_entero
LET cnt_hasta = LENGTH(vmonto_string)

FOR cnt = cnt_hasta TO 1 STEP -1 -- TO cnt_hasta

    CALL fun_desglosa(vmonto_string) RETURNING vnumero, vnuevo_monto_string
    
    IF  LENGTH(vmonto_string) = 12 OR
        LENGTH(vmonto_string) = 9 OR
        LENGTH(vmonto_string) = 6 OR
        LENGTH(vmonto_string) = 3 THEN
        
        IF vnumero = "1" THEN
            IF vmonto_string[2,3] = "00" THEN
                LET vbandera = 1
            ELSE
                LET vbandera = 0
            END IF
        ELSE
            LET vbandera = 0
        END IF
        
        CALL fcentenas(vnumero,vbandera) RETURNING vtexto
        IF valor_letras IS NULL THEN
            LET valor_letras = vtexto
        ELSE
            LET valor_letras = valor_letras ,vtexto
        END IF
        LET vmonto_string = vnuevo_monto_string
    ELSE
  
    --decenas
    IF  LENGTH(vmonto_string) = 11 OR
        LENGTH(vmonto_string) = 8 OR
        LENGTH(vmonto_string) = 5 OR
        LENGTH(vmonto_string) = 2 THEN
       
        IF vnumero = "1" OR vnumero = "2" THEN
            IF vmonto_string[2] = "0" THEN
                LET vbandera = 1
            ELSE
                LET vbandera = 0
            END IF
        ELSE
            IF vmonto_string[2] = "0" THEN
                LET vbandera = 0
            ELSE 
                LET vbandera = 1
            END IF
        END IF
        CASE 
        WHEN vmonto_string[1,2] = "11" 
            LET vtexto = "ONCE "
            IF valor_letras IS NULL THEN
                LET valor_letras = vtexto
            ELSE
                LET valor_letras = valor_letras ,vtexto
            END IF
            LET vmonto_string = vnuevo_monto_string
            CALL fun_desglosa(vmonto_string) RETURNING vnumero, vnuevo_monto_string
            IF LENGTH(vmonto_string) = 10 OR
                LENGTH(vmonto_string) = 4 THEN
                LET valor_letras = valor_letras, "MIL "
            END IF
            IF LENGTH(vmonto_string) = 7 THEN
                IF vnumero = "1" THEN
                    LET valor_letras = valor_letras, "MILLON "
                ELSE
                    LET valor_letras = valor_letras, "MILLONES "
                END IF
            END IF

            LET vmonto_string = vnuevo_monto_string
        
        WHEN vmonto_string[1,2] = "12" 
            LET vtexto = "DOCE "
            IF valor_letras IS NULL THEN
                LET valor_letras = vtexto
            ELSE
                LET valor_letras = valor_letras ,vtexto
            END IF
            LET vmonto_string = vnuevo_monto_string
            CALL fun_desglosa(vmonto_string) RETURNING vnumero, vnuevo_monto_string
            IF LENGTH(vmonto_string) = 10 OR
                LENGTH(vmonto_string) = 4 THEN
                LET valor_letras = valor_letras, "MIL "
            END IF
            IF LENGTH(vmonto_string) = 7 THEN
                IF vnumero = "1" THEN
                    LET valor_letras = valor_letras, "MILLON "
                ELSE
                    LET valor_letras = valor_letras, "MILLONES "
                END IF
            END IF
            LET vmonto_string = vnuevo_monto_string
            
        WHEN vmonto_string[1,2] = "13" 
            LET vtexto = "TRECE "
            IF valor_letras IS NULL THEN
                LET valor_letras = vtexto
            ELSE
                LET valor_letras = valor_letras ,vtexto
            END IF
            LET vmonto_string = vnuevo_monto_string
            CALL fun_desglosa(vmonto_string) RETURNING vnumero, vnuevo_monto_string
            IF LENGTH(vmonto_string) = 10 OR
                LENGTH(vmonto_string) = 4 THEN
                LET valor_letras = valor_letras, "MIL "
            END IF
            IF LENGTH(vmonto_string) = 7 THEN
                IF vnumero = "1" THEN
                    LET valor_letras = valor_letras, "MILLON "
                ELSE
                    LET valor_letras = valor_letras, "MILLONES "
                END IF
            END IF

            LET vmonto_string = vnuevo_monto_string
            
        WHEN vmonto_string[1,2] = "14" 
            LET vtexto = "CATORCE "
            IF valor_letras IS NULL THEN
                LET valor_letras = vtexto
            ELSE
                LET valor_letras = valor_letras ,vtexto
            END IF
            LET vmonto_string = vnuevo_monto_string
            CALL fun_desglosa(vmonto_string) RETURNING vnumero, vnuevo_monto_string
            LET vmonto_string = vnuevo_monto_string
            
        WHEN vmonto_string[1,2] = "15" 
            LET vtexto = "QUINCE "
            IF valor_letras IS NULL THEN
                LET valor_letras = vtexto
            ELSE
                LET valor_letras = valor_letras ,vtexto
            END IF
            LET vmonto_string = vnuevo_monto_string
            CALL fun_desglosa(vmonto_string) RETURNING vnumero, vnuevo_monto_string
            IF LENGTH(vmonto_string) = 10 OR
                LENGTH(vmonto_string) = 4 THEN
                LET valor_letras = valor_letras, "MIL "
            END IF
            IF LENGTH(vmonto_string) = 7 THEN
                IF vnumero = "1" THEN
                    LET valor_letras = valor_letras, "MILLON "
                ELSE
                    LET valor_letras = valor_letras, "MILLONES "
                END IF
            END IF

            LET vmonto_string = vnuevo_monto_string
            
        OTHERWISE
         CALL fdecenas(vnumero,vbandera) RETURNING vtexto
        
        IF valor_letras IS NULL THEN
            LET valor_letras = vtexto
        ELSE
            LET valor_letras = valor_letras ,vtexto
        END IF
        LET vmonto_string = vnuevo_monto_string
        END CASE
    ELSE
    IF  LENGTH(vmonto_string) = 10 OR
        LENGTH(vmonto_string) = 7 OR
        LENGTH(vmonto_string) = 4 OR
        LENGTH(vmonto_string) = 1 THEN
        
        IF vnumero = "1" AND LENGTH(vmonto_string) = 1 THEN
            LET vbandera = 0
        ELSE
            LET vbandera = 1
        END IF 
        
        CALL funidades(vnumero,vbandera) RETURNING vtexto
        IF valor_letras IS NULL THEN
            LET valor_letras = vtexto
        ELSE
            LET valor_letras = valor_letras ,vtexto
        END IF
        IF LENGTH(vmonto_string) = 10 OR
        LENGTH(vmonto_string) = 4 THEN
            LET valor_letras = valor_letras, "MIL "
        END IF
        IF LENGTH(vmonto_string) = 7 THEN
            IF vnumero = "1" THEN
                LET valor_letras = valor_letras, "MILLON "
            ELSE
                LET valor_letras = valor_letras, "MILLONES "
            END IF
        END IF
        LET vmonto_string = vnuevo_monto_string
        
    END IF
    END IF
    END IF
END FOR

IF vmoneda = "GTQ" THEN 
    LET valor_letras = valor_letras CLIPPED, " QUETZALES "
ELSE
    IF vmoneda = "USD" THEN
        LET valor_letras = valor_letras CLIPPED, " DOLARES "
    END IF
END IF 
IF vcentavos > 0 THEN
    LET valor_letras = valor_letras, "CON ",vcentavos USING "&&","/100"
ELSE
    LET valor_letras = valor_letras, "CON 00/100"
END IF

RETURN valor_letras
END FUNCTION

FUNCTION fun_desglosa(vmonto_string VARCHAR(12)) 
DEFINE vnumero SMALLINT
DEFINE vnuevo_monto_string VARCHAR(12)
DEFINE vmonto_resta VARCHAR(12)
DEFINE valor1, valor2, valor3 INTEGER
DEFINE cnt SMALLINT
DEFINE vdiferencia_digitos SMALLINT

INITIALIZE valor1, valor2, valor3, vnuevo_monto_string,vmonto_resta,vnumero, vdiferencia_digitos TO NULL

LET vnumero = vmonto_string[1]
LET vmonto_resta = vnumero
FOR cnt = 2 TO LENGTH(vmonto_string)
    LET vmonto_resta = vmonto_resta,"0"
END FOR

LET valor1 = vmonto_string
LET valor2 = vmonto_resta
LET valor3 = valor1 - valor2
LET vnuevo_monto_string = valor3
LET vdiferencia_digitos = LENGTH(vmonto_string) - LENGTH(vnuevo_monto_string)
IF vdiferencia_digitos > 1 THEN
    FOR cnt = 1 TO vdiferencia_digitos-1
        LET vnuevo_monto_string = "0",vnuevo_monto_string
    END FOR
END IF
RETURN vnumero, vnuevo_monto_string
END FUNCTION
 
FUNCTION funidades(vvalor SMALLINT,vbandera SMALLINT)
DEFINE vvalorletra VARCHAR(50)

CASE 
WHEN vvalor = 0
    LET vvalorletra = " "
WHEN vvalor = 1 
    IF vbandera = 1 THEN
        LET vvalorletra = "UN "
    ELSE
        LET vvalorletra = "UNO "
    END IF
WHEN vvalor = 2
    LET vvalorletra = "DOS "
WHEN vvalor = 3
    LET vvalorletra = "TRES "
WHEN vvalor = 4
    LET vvalorletra = "CUATRO "
WHEN vvalor = 5
    LET vvalorletra = "CINCO "
WHEN vvalor = 6
    LET vvalorletra = "SEIS "
WHEN vvalor = 7
    LET vvalorletra = "SIETE "
WHEN vvalor = 8
    LET vvalorletra = "OCHO "
WHEN vvalor = 9
    LET vvalorletra = "NUEVE "
END CASE

RETURN vvalorletra
END FUNCTION 

FUNCTION fdecenas(vvalor SMALLINT,vbandera SMALLINT)
DEFINE vvalorletra VARCHAR(50)

CASE 
WHEN vvalor = 1 
    IF vbandera = 1 THEN
        LET vvalorletra = "DIEZ"
    ELSE
        LET vvalorletra = "DIECI"
    END IF
WHEN vvalor = 2
    IF vbandera = 1 THEN
        LET vvalorletra = "VEINTE "
    ELSE
        LET vvalorletra = "VEINTI"
    END IF
WHEN vvalor = 3
    LET vvalorletra = "TREINTA "
    IF vbandera = 1 THEN
        LET vvalorletra = vvalorletra, "Y "
    END IF
WHEN vvalor = 4
    LET vvalorletra = "CUARENTA "
    IF vbandera = 1 THEN
        LET vvalorletra = vvalorletra, "Y "
    END IF
WHEN vvalor = 5
    LET vvalorletra = "CINCUENTA "
    IF vbandera = 1 THEN
        LET vvalorletra = vvalorletra, "Y "
    END IF
WHEN vvalor = 6
    LET vvalorletra = "SESENTA "
    IF vbandera = 1 THEN
        LET vvalorletra = vvalorletra, "Y "
    END IF
WHEN vvalor = 7
    LET vvalorletra = "SETENTA "
    IF vbandera = 1 THEN
        LET vvalorletra = vvalorletra, "Y "
    END IF
WHEN vvalor = 8
    LET vvalorletra = "OCHENTA "
    IF vbandera = 1 THEN
        LET vvalorletra = vvalorletra, "Y "
    END IF
WHEN vvalor = 9
    LET vvalorletra = "NOVENTA "
    IF vbandera = 1 THEN
        LET vvalorletra = vvalorletra, "Y "
    END IF
END CASE

RETURN vvalorletra
END FUNCTION

FUNCTION fcentenas(vvalor INTEGER,vbandera SMALLINT)
DEFINE vvalorletra VARCHAR(50)
DEFINE vvalorstring STRING

LET vvalorstring = vvalor

CASE 
WHEN vvalor = 1 
    IF vbandera = 1 THEN
        LET vvalorletra = "CIEN "
    ELSE
        LET vvalorletra = "CIENTO "
    END IF
WHEN vvalor = 2
    LET vvalorletra = "DOSCIENTOS "
WHEN vvalor = 3
    LET vvalorletra = "TRESCIENTOS "
WHEN vvalor = 4
    LET vvalorletra = "CUATROCIENTOS "
WHEN vvalor = 5
    LET vvalorletra = "QUINIENTOS "
WHEN vvalor = 6
    LET vvalorletra = "SEISCIENTOS "
WHEN vvalor = 7
    LET vvalorletra = "SETECIENTOS "
WHEN vvalor = 8
    LET vvalorletra = "OCHOCIENTOS "
WHEN vvalor = 9
    LET vvalorletra = "NOVECIENTOS "
END CASE


RETURN vvalorletra
END FUNCTION

REPORT imprime_factura(rep_datos_factura)
DEFINE rep_datos_factura RECORD
    reg_company RECORD
        x_studio_nit VARCHAR(12),
        x_studio_afiliacion_iva VARCHAR(100),
        x_studio_nombre_comercial VARCHAR(70),
        x_studio_codigo_establecimiento VARCHAR(3),
        x_studio_nombre_del_establecimiento VARCHAR(70),
        x_studio_email_fel VARCHAR(50),
        street VARCHAR(100),
        street2 VARCHAR(100),
        zip VARCHAR(100),
        city VARCHAR(100),
        state_id VARCHAR(100),
        country_id VARCHAR(100),
        direccion_establecimiento VARCHAR(70),
        departamento_establecimiento VARCHAR(20),
        municipio_establecimiento VARCHAR(20),
        pais_establecimiento VARCHAR(2),
        codigo_postal_establecimiento INTEGER,
        exportador_nombre VARCHAR(100),
        exportador_codigo VARCHAR(25)
    END RECORD,
    reg_factura RECORD
        serie VARCHAR(20),
        numero INTEGER,
        diario VARCHAR(100),
        tipo_factura VARCHAR(12),
        fecha_factura DATE,
        fecha_emision VARCHAR(35),
        fecha_anulacion VARCHAR(35),
        estado_factura VARCHAR(20),
        moneda VARCHAR(3),
        tipo_cambio DECIMAL(12,8),
        monto_base DECIMAL(18,2),
        monto_iva DECIMAL(18,2),
        monto_descuento DECIMAL(18,2),
        monto_total DECIMAL(18,2),
        estado_certificado VARCHAR(100),
        id_factura VARCHAR(100),
        total_en_letras VARCHAR(255),
        nombre_consignatario_destinatario VARCHAR(100),
        direccion_consignatario_destinatario VARCHAR(100),
        incoterm VARCHAR(100),
        serial VARCHAR(100),
        es_exportacion SMALLINT,
        tipo_documento VARCHAR(100),
        estado_del_documento VARCHAR(20),
        forma_de_pago VARCHAR(100),
        vendedor VARCHAR(100),
        numero_pedido VARCHAR(100),
        ruta VARCHAR(100),
        total_de_productos SMALLINT,
        observaciones VARCHAR(255),
        factura_envio VARCHAR(100),
        autorizacion_fel VARCHAR(100),
        serie_fel VARCHAR(20),
        numero_fel VARCHAR(40),
        estado_fel CHAR(2),
        fecha_fel DATETIME YEAR TO SECOND,
        fecha_feltxt VARCHAR(35),
        es_exenta SMALLINT,  --Local excenta=1, exportacion=0, local con iva= 0
        posicion_fiscal SMALLINT,
        cod_frase SMALLINT,
        cod_escenario SMALLINT,
        certificador_nit VARCHAR(50),
        certificador_nombre VARCHAR(100),
        numero_interno VARCHAR(100),
        proyecto INTEGER
    END RECORD,
    reg_cliente RECORD
        email_cliente VARCHAR(50),
        email_adicional VARCHAR(50),
        nit_cliente VARCHAR(20),
        tipo_documento_identificacion SMALLINT,
        nombre_cliente VARCHAR(100),
        street VARCHAR(100),
        street2 VARCHAR(100),
        direccion_cliente VARCHAR(150),
        nombre_departamento_cliente VARCHAR(20),
        municipio_cliente VARCHAR(50),
        pais_cliente VARCHAR(2),
        codigo_postal_cliente SMALLINT,
        vendedor_id INTEGER
    END RECORD,
    reg_det_factura  RECORD
        numero_linea SMALLINT,
        product_id   INTEGER,
        codigo_producto VARCHAR(100),
        nombre_producto VARCHAR(200),
        unidad_medida   VARCHAR(100),
        cantidad        DECIMAL(12,3),
        precio_unitario DECIMAL(18,6),
        valor_total_sin_descuento DECIMAL(18,2),
        descuento   DECIMAL(18,2),
        valor_total_despues_descuento DECIMAL(18,2),
        valor_total_despues_descuento_sin_iva DECIMAL(18,2),
        valor_del_iva DECIMAL(18,2),
        tipo_producto VARCHAR(100),
        nombre_impuesto VARCHAR(100)
    END RECORD
END RECORD

    FORMAT 
    FIRST PAGE HEADER
       
        PRINTX rep_datos_factura.reg_company.*, rep_datos_factura.reg_cliente.*, rep_datos_factura.reg_factura.*

    ON EVERY ROW
       PRINTX rep_datos_factura.reg_det_factura.* 


END REPORT